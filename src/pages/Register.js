import {useState, useEffect} from 'react';
import {useNavigate} from 'react-router-dom';

import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import Swal from 'sweetalert2';

export default function Register() {

	const navigate = useNavigate();
	
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [isActive, setIsActive] = useState(false);
	const [matchMessage, setMatchMessage] = useState(" ");

	useEffect(() => {
	    if(password1 !== password2){
	    	setMatchMessage("Password and Confirm password doesn't match")
	    }
	    else{
	    	setMatchMessage(" ")
	    }

	}, [password2, password1])

	useEffect(() => {
	    if((firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
	                setIsActive(true);
	            } else {
	                setIsActive(false);
	            }

	    }, [firstName, lastName, email, mobileNo, password1, password2])

	function registerUser(e){
		e.preventDefault()
		console.log(firstName)
		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
		  method: 'POST',
		  headers: {
		    'Content-Type': 'application/json'
		  },
		  body: JSON.stringify({
		    "firstName": firstName,
		    "lastName": lastName,
		    "email": email,
		    "mobileNo": mobileNo,
		    "password": password1
		  })
		})
		.then(res => res.json())
		.then(data => {

			if(data === false){
				Swal.fire({
				title: "Error",
				icon: "error",
				text: "Error"
				})
			}
			else if(data === true){
				Swal.fire({
				  position: 'center',
				  icon: 'success',
				  title: 'Thank you for registering!',
				  timer: 1500
				})
				setFirstName("");
				setLastName("");
				setEmail("");
				setMobileNo("");
				setPassword1("");
				setPassword2("");
				navigate('/login')
			}
			else{
				Swal.fire({
				icon: "error",
				text: (data.message)
				})
			}
		})
	}
		
	return(
		<Container fluid className="registerWrapper pt-4">
			<Row className="justify-content-center">
				<Col lg={8} className="registerHeadingWrapper pb-5">
					<h5 className="registerHeading">Create an Account</h5>
				</Col>
			</Row>
			<Row className="justify-content-center pt-5">
				<Col lg={4}>
					<Form onSubmit={registerUser}>
						<Form.Group controlId="userFirstName">
						  <Form.Label className="registerLabel">First Name</Form.Label>
						  <Form.Control className="registerInput" type="name" placeholder="Enter first name"
						   value={firstName}
						   onChange={e => setFirstName(e.target.value)} 
						   required/>
						</Form.Group>

						<Form.Group className="pt-3" controlId="userLastName">
						  <Form.Label className="registerLabel">Last Name</Form.Label>
						  <Form.Control className="registerInput" type="name" placeholder="Enter last name" 
						  value={lastName}
						  onChange={e => setLastName(e.target.value)} 
						  required/>
						</Form.Group>

					  <Form.Group  className="pt-3" controlId="userEmail">
					    <Form.Label className="registerLabel">Email address</Form.Label>
					    <Form.Control className="registerInput" type="email" placeholder="Enter email" 
					    value={email}
					    onChange={e => setEmail(e.target.value)} 
					    required/>
					  </Form.Group>

					  <Form.Group  className="pt-3" controlId="userMobileNo">
					    <Form.Label className="registerLabel">Mobile No.</Form.Label>
					    <Form.Control className="registerInput" type="number" placeholder="Enter mobile number" 
					    value={mobileNo}
					    onChange={e => setMobileNo(e.target.value)} 
					    required/>
					  </Form.Group>

					  <Form.Group className="pt-3" controlId="userPassword1">
					    <Form.Label className="registerLabel">Password</Form.Label>
					    <Form.Control className="registerInput" type="password" placeholder="Enter password" 
					    value={password1}
					    onChange={e => setPassword1(e.target.value)} 
					    required/>
					  </Form.Group>

					  <Form.Group className="pt-3" controlId="userPassword2">
					    <Form.Label className="registerLabel">Confirm Password</Form.Label>
					    <Form.Control className="registerInput" type="password" placeholder="Enter password again" 
					    value={password2}
					    onChange={e => setPassword2(e.target.value)} 
					    required/>
					  </Form.Group>

					  <Form.Text className="text-muted">
					    <div className="matchMessage">{matchMessage}</div>
					  </Form.Text>
					  <Row className="justify-content-center">
					  	<Col lg={3}>
					  { isActive ? 
					  <Button className="mt-5 signUpButton" variant="primary" type="submit" id="submitBtn">
					  Sign up
					  </Button>
					  :
					  <Button className="mt-5 signUpButton" variant="primary" type="submit" id="submitBtn" disabled>
					  Sign up
					  </Button>
					  }
					  	</Col>
					  </Row>
					</Form>
				</Col>
			</Row>
		</Container>
	)
}