import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import {useNavigate} from 'react-router-dom';

import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import Swal from 'sweetalert2';

export default function Login() {

	const navigate = useNavigate();

	const {setCartRefresh} = useContext(UserContext)
	const {setUser} = useContext(UserContext)

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
	    if( email !== '' && password !== ''){
	        setIsActive(true);
	    } else {
	        setIsActive(false);
	    }

	}, [email, password])

	function loginUser (e){
	  e.preventDefault()

	  fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
	    method: 'POST',
	    headers: {
	      'Content-Type': 'application/json'
	    },
	    body: JSON.stringify({
	      "email": email,
	      "password": password
	    })
	  })
	  .then(res => res.json())
	  .then(data => {
	    if(data === false){
	      Swal.fire({
	        title: "Email is not yet registered",
	        icon: "error",
	        text: "Please verify email address"
	      })
	    }
	    else if(typeof data.access == "undefined"){
	      Swal.fire({
	        title: data.message,
	        icon: "error",
	        text: "Please verify password"
	      })
	    }
	    else{
	      localStorage.setItem('token', data.access);
	      retrieveUserDetails(data.access);
	      Swal.fire({
	        position: 'center',
	        icon: 'success',
	        title: 'Login Successful',
	        showConfirmButton: false,
	        timer: 1500
	      })
	      localStorage.removeItem('cart');
	      navigate('/')
	      setCartRefresh(current => !current)
	    }
	  })
	}

	const retrieveUserDetails = async (token) => {
	  await fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
	    headers: {
	      Authorization: `Bearer ${token}`
	    }
	    })
	    .then(res => res.json())
	    .then(data => {
	      setUser({
	        id: data._id,
	        firstName: data.firstName,
	        lastName: data.lastName,
	        email: data.email,
	        mobileNo: data.mobileNo,
	        isAdmin: data.isAdmin
	      })

	    })
	}

	return(
		<Container fluid className="loginWrapper">
			<Row className="justify-content-center">
				<Col lg={8} className="registerHeadingWrapper pb-5">
					<h5 className="registerHeading">Log In</h5>
				</Col>
			</Row>
			<Row className="justify-content-center pt-5">
				<Col lg={4}>
					<Form onSubmit={loginUser}>

					  <Form.Group  className="pt-3" controlId="userEmail">
					    <Form.Label className="registerLabel">Email address</Form.Label>
					    <Form.Control className="registerInput" type="email" placeholder="Enter email" 
					    value={email}
					    onChange={e => setEmail(e.target.value)} 
					    required/>
					  </Form.Group>


					  <Form.Group className="pt-3" controlId="userPassword">
					    <Form.Label className="registerLabel">Password</Form.Label>
					    <Form.Control className="registerInput" type="password" placeholder="Enter password" 
					    value={password}
					    onChange={e => setPassword(e.target.value)} 
					    required/>
					  </Form.Group>

					  <Row className="justify-content-center">
					  	<Col lg={3}>
					  { isActive ? 
					  <Button className="mt-5 signUpButton" variant="primary" type="submit" id="submitBtn">
					  Sign in
					  </Button>
					  :
					  <Button className="mt-5 signUpButton" variant="primary" type="submit" id="submitBtn" disabled>
					  Sign in
					  </Button>
					  }
					  	</Col>
					  </Row>
					</Form>
				</Col>
			</Row>
		</Container>
	)
}