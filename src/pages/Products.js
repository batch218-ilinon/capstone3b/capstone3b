import ProductCard from '../components/ProductCard'
import { useState, useEffect } from 'react';

import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';

import Form from 'react-bootstrap/Form';

import Button from 'react-bootstrap/Button';
import Offcanvas from 'react-bootstrap/Offcanvas';

export default function Products() {

	const productBrand = ["Acer", "Apple", "Asus", "Dell", "Huawei", "Lenovo"]
	const productRam = [8, 16, 24]
	const [checkedBrand, setCheckedBrand] = useState([])
	const [checkedRam, setCheckedRam] = useState([])
	const [activeProductUpdate, setActiveProductUpdate] = useState(false)
	const [activeProducts, setActiveProducts] = useState([])
	const [currentProducts, setCurrentProducts] = useState([])
	const [productCards, setProductCards] = useState([])
	const [brandCheckbox, setBrandCheckbox] = useState([])
	const [RamCheckbox, setRamCheckbox] = useState([])
	const [sortBy, setSortBy] = useState("Sort By")

	const [refreshFilter, setRefreshFilter] = useState(false)

	const [ramIsChecked, setRamIsChecked] = useState(
	    new Array(productRam.length).fill(false)
	);
	const [brandIsChecked, setBrandIsChecked] = useState(
	    new Array(productBrand.length).fill(false)
	);

	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/product/active`)
		.then(res => res.json())
		.then(data => {
			setRamCheckbox(productRam.map((Ram, index) => {
				return (
					<Form.Check 
					  key = {`${Ram}`}
					  type='checkbox'
					  checked={ramIsChecked[index]}
					  id= {`Ram${Ram}Checkbox`}
					  label={`${Ram} GB`}
					  onChange={() => filterRAM(`${index}`)}
					/>
				)
			}))
			setBrandCheckbox(productBrand.map((brand, index) => {
				return (
					<Form.Check 
					  key = {`${brand}`}
					  type='checkbox'
					  checked={brandIsChecked[index]}
					  id= {`${brand}Checkbox`}
					  label={brand}
					  onChange={() => filterBrand(`${index}`)}
					/>
				)
			}))
			setProductCards(data.map(product => {
				return <ProductCard key={product._id} product = {product}/>
			}))
			setActiveProducts(data)
			setCurrentProducts(data)
		})
	}, [])

	useEffect(() => {
		selectSort(sortBy)
	}, [currentProducts])

	useEffect(() => {
		setRamCheckbox(productRam.map((Ram, index) => {
			return (
				<Form.Check 
				  key = {`${Ram}`}
				  type='checkbox'
				  checked={ramIsChecked[index]}
				  id= {`Ram${Ram}Checkbox`}
				  label={`${Ram} GB`}
				  onChange={() => filterRAM(`${index}`)}
				/>
			)
		}))
		setBrandCheckbox(productBrand.map((brand, index) => {
			return (
				<Form.Check 
				  key = {`${brand}`}
				  type='checkbox'
				  checked={brandIsChecked[index]}
				  id= {`${brand}Checkbox`}
				  label={brand}
				  onChange={() => filterBrand(`${index}`)}
				/>
			)
		}))
	}, [refreshFilter])


	function selectSort(event){
		setSortBy(event)
		if(event === "Best Sellers"){
			let currentProductsTemp = currentProducts
			currentProductsTemp.sort((p1, p2) => 
				p2.sellCount - p1.sellCount
			);
			setProductCards(currentProductsTemp.map(product => {
				return <ProductCard key={product._id} product = {product}/> 
			}))
		}
		else if(event === "Newest"){
			let currentProductsTemp = currentProducts
			currentProductsTemp.sort((p1, p2) => 
				(p1.createdOn < p2.createdOn) ? 1 : (p1.createdOn > p2.createdOn) ? -1 : 0);;
			setProductCards(currentProductsTemp.map(product => {
				return <ProductCard key={product._id} product = {product}/> 
			}))
		}
		else if(event === "Price, low to high"){
			let currentProductsTemp = currentProducts
			currentProductsTemp.sort((p1, p2) => 
				p1.price - p2.price
			);
			setProductCards(currentProductsTemp.map(product => {
				return <ProductCard key={product._id} product = {product}/> 
			}))
		}
		else{
			let currentProductsTemp = currentProducts
			currentProductsTemp.sort((p1, p2) => 
				 p2.price - p1.price
			);
			setProductCards(currentProductsTemp.map(product => {
				return <ProductCard key={product._id} product = {product}/> 
			}))
		}

	}

	useEffect(() => {
		console.log(checkedRam)
		console.log(checkedBrand)
		if(checkedRam.length === 0 && checkedBrand.length === 0){
			setCurrentProducts(activeProducts)
			setProductCards(activeProducts.map(product => {
				return <ProductCard key={product._id} product = {product}/>
			}))
		}
		else if(checkedRam.length === 0 && checkedBrand.length > 0){
			let productsTemp = []
			for(let productIndex = 0; productIndex < activeProducts.length; productIndex++){
				for(let brandIndex = 0; brandIndex < checkedBrand.length; brandIndex++){
					if(checkedBrand[brandIndex] === activeProducts[productIndex].brand){
						productsTemp.push(activeProducts[productIndex])
					}
				}
			}
			setCurrentProducts(productsTemp)
			setProductCards(productsTemp.map(product => {
				return <ProductCard key={product._id} product = {product}/>
			}))
		}
		else if(checkedRam.length > 0 && checkedBrand.length === 0){
			let productsTemp = []
			for(let productIndex = 0; productIndex < activeProducts.length; productIndex++){
				for(let RamIndex = 0; RamIndex < checkedRam.length; RamIndex++){
					if(checkedRam[RamIndex] === activeProducts[productIndex].ram){
						productsTemp.push(activeProducts[productIndex])
					}
				}
			}
			setCurrentProducts(productsTemp)
			setProductCards(productsTemp.map(product => {
				return <ProductCard key={product._id} product = {product}/>
			}))
		}
		else{
			let productsTemp = []
			for(let productIndex = 0; productIndex < activeProducts.length; productIndex++){
				for(let brandIndex = 0; brandIndex < checkedBrand.length; brandIndex++){
					if(checkedBrand[brandIndex] === activeProducts[productIndex].brand){
						productsTemp.push(activeProducts[productIndex])
					}
				}
			}
			let productList = []
			for(let productIndex = 0; productIndex < productsTemp.length; productIndex++){
				for(let RamIndex = 0; RamIndex < checkedRam.length; RamIndex++){
					if(checkedRam[RamIndex] === productsTemp[productIndex].ram){
						productList.push(productsTemp[productIndex])
					}
				}
			}
			setCurrentProducts(productList)
			setProductCards(productList.map(product => {
				return <ProductCard key={product._id} product = {product}/>
			}))
		}
	}, [activeProductUpdate])


	function filterBrand(index){
		let checkBoxTemp = brandIsChecked
		checkBoxTemp[index] = !checkBoxTemp[index]
		setBrandIsChecked(checkBoxTemp)
		setRefreshFilter(current => !current)

		let checkedBrandTemp = checkedBrand
		let checkedBrandIndex = checkedBrand.indexOf(productBrand[index])
		if(checkedBrandIndex === -1){
			checkedBrandTemp.push(productBrand[index]) 
			setCheckedBrand(checkedBrandTemp)
			setActiveProductUpdate(current => !current)
		}
		else{
			checkedBrandTemp.splice(checkedBrandIndex, 1)
			setCheckedBrand(checkedBrandTemp)
			setActiveProductUpdate(current => !current)
		}
	}

	function filterRAM(index){
		let checkBoxTemp = ramIsChecked
		checkBoxTemp[index] = !checkBoxTemp[index]
		console.log(checkBoxTemp)
		setRamIsChecked(checkBoxTemp)
		setRefreshFilter(current => !current)

		let checkedRamTemp = checkedRam
		let checkedRamIndex = checkedRam.indexOf(productRam[index])
		if(checkedRamIndex === -1){
			checkedRamTemp.push(productRam[index]) 
			setCheckedRam(checkedRamTemp)
			setActiveProductUpdate(current => !current)
		}
		else{
			checkedRamTemp.splice(checkedRamIndex, 1)
			setCheckedRam(checkedRamTemp)
			setActiveProductUpdate(current => !current)
		}
	}


	return(
		<>
		<Container fluid className="productPageWrapper">
			<Row className="justify-content-center">
				<Col md={11} lg={10}>
					<Row className="justify-content-between pb-4 align-items-center">
						<Col xs={4} md={2}  lg={2} xl={2} className="d-lg-none">
							<h6 className="filterTitle mb-0" onClick={handleShow}>Filter</h6>
						</Col>
						<Col xs={4} md={2}  lg={2} xl={2} className="d-none d-lg-block">
							<h6 className="filterTitle mb-0">Filter</h6>
						</Col>
						<Col xs={4} md={2} lg={2} xl={2} className="text-end">
							<DropdownButton 
								title={sortBy} 
								drop="down-centered" 
								id="dropdown-product-button"
								onSelect={selectSort}
							>
							  <Dropdown.Item eventKey="Best Sellers">Best Sellers</Dropdown.Item>
							  <Dropdown.Item eventKey="Newest">Newest</Dropdown.Item>
							  <Dropdown.Item eventKey="Price, low to high">Price, low to high</Dropdown.Item>
							  <Dropdown.Item eventKey="Price, high to low">Price, high to low</Dropdown.Item>
							</DropdownButton>
						</Col>
					</Row>
				</Col>
			</Row>
			<Row className="justify-content-center">
				<Col md={11} lg={10}>
					<Row className="justify-content-between">
						<Col md={4} lg={3} xl={2} className={"d-none d-lg-block"}>
							<div className="filterBrandTitle pl-3">Product Brand</div>
							<div className="py-3 pl-3 checkboxWrapper">
								{brandCheckbox}
							</div>
							<div className="filterRamTitle pl-3 mt-4">Product RAM</div>
							<div className="py-3 pl-3 checkboxWrapper">
								{RamCheckbox}
							</div>
						</Col>
						<Col lg={9} xl={9}>
							<Row>
							{productCards}
							</Row>
						</Col>	
					</Row>
				</Col>
			</Row>
		</Container>
		<Offcanvas show={show} onHide={handleClose} placement='start' className="d-lg-none">
		  <Offcanvas.Header closeButton>
		    <Offcanvas.Title className="cartTitle">Filter By</Offcanvas.Title>
		  </Offcanvas.Header>
		  <Offcanvas.Body>
		  		<div className="filterBrandTitle pl-3">Product Brand</div>
		  		<div className="py-3 pl-3 checkboxWrapper">
		  			{brandCheckbox}
		  		</div>
		  		<div className="filterRamTitle pl-3 mt-4">Product RAM</div>
		  		<div className="py-3 pl-3 checkboxWrapper">
		  			{RamCheckbox}
		  		</div>
		  </Offcanvas.Body>
		</Offcanvas>
		</>
	)
}