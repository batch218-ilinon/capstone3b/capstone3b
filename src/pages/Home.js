import HomeCarousel from '../components/HomeCarousel'
import TopSelling from '../components/TopSelling'
import NewArrival from '../components/NewArrival'

import {useNavigate} from 'react-router-dom';

export default function Home(){
	const navigate = useNavigate();
	return(
		<div className="homePageWrapper">
		<HomeCarousel />
		<TopSelling onClick={() => navigate('/product')}/>
		<NewArrival onClick={() => navigate('/product')}/>
		</div>
	)
}