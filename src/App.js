import Home from './pages/Home'
import Register from './pages/Register'
import Login from './pages/Login'
import Products from './pages/Products'
import Logout from './pages/Logout'

import Header from './components/Header'
import Footer from './components/Footer'

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import {UserProvider} from './UserContext'
import React, {useState, useEffect} from 'react';

function App() {

  const [cartRefresh, setCartRefresh] = useState(false)
  
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
      const token = localStorage.getItem('token')
      if(token !== null){
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if(typeof data._id !== "undefined") {
                setUser({
                    id: data._id,
                    firstName: data.firstName,
                    lastName: data.lastName,
                    email: data.email,
                    mobileNo: data.mobileNo,
                    isAdmin: data.isAdmin
                })
            } 
            else { 
                setUser({
                    id: null,
                    isAdmin: null
                })
            }

        })
      }
  }, []);

  return (
    <>
    <UserProvider value={{cartRefresh, setCartRefresh, unsetUser, user, setUser}}>
    <Router>
      <Header />
      <Routes>
        <Route path = "/" element= {<Home />} />
        <Route path = "/register" element= {<Register />} />
        <Route path = "/login" element= {<Login />} />
        <Route path = "/products" element= {<Products />} />
        <Route path = "/logout" element= {<Logout />} />
      </Routes>
      <Footer />
    </Router>
    </UserProvider>
    </>
  );
}

export default App;
