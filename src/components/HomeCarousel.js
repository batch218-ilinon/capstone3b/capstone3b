import Carousel from 'react-bootstrap/Carousel';

import image1 from '../images/windows11.jpeg';
import image2 from '../images/intel.jpg';
import image3 from '../images/amd.jpg';
import image1XS from '../images/windows11md.jpeg'

export default function HomeCarousel() {
  return (
    <>
      <Carousel slide={false} className="mt-3 d-none d-md-block">
            <Carousel.Item interval={2500}>
              <img
                className="d-block w-100"
                src={image1}
                alt="First slide"
              />
            </Carousel.Item>
            <Carousel.Item interval={2500}>
              <img
                className="d-block w-100"
                src={image2}
                alt="Second slide"
              />
            </Carousel.Item>
            <Carousel.Item interval={2500}>
              <img
                className="d-block w-100"
                src={image3}
                alt="Third slide"
              />
            </Carousel.Item>
      </Carousel>
      <img
        className="d-block w-100 d-md-none"
        src={image1XS}
        alt="Windows 11"
      />
    </>
  );
}