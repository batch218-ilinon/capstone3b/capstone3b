
export default function ProductSlide({product}) {
	return(
		<div className="productCard text-center">
			<img className="productImage" src={product.imgSrc} />			
			<div className="productName text-center">{product.name}</div>
			<div className="productPrice text-center">&#8369;{product.price}</div>
		</div>
	)
}