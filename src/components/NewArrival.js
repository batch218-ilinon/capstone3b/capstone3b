import ProductSlide from './ProductSlide'
import {useState, useEffect} from 'react';
import buttonLeft from '../icons/buttonLeft.svg';
import buttonRight from '../icons/buttonRight.svg';
import LoadingSpinner from './LoadingSpinner'

import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import "swiper/css";
import "swiper/css/navigation";

import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Keyboard, Scrollbar, Navigation, Pagination } from "swiper";
import {useNavigate} from 'react-router-dom';

export default function NewArrival() {

	const [topItems, setTopItems] = useState([]);
	const navigate = useNavigate();
	const [loading, setLoading] = useState(false);

	function toProduct(){
		navigate('/products')
	}

	useEffect(() => {
		setLoading(true)
		fetch(`${process.env.REACT_APP_API_URL}/product/newest`, {
		  method: 'GET',
		  headers: {
		    'Content-Type': 'application/json',
		    Authorization: `Bearer ${localStorage.getItem('item')}`
		  }
		})
		.then(res => res.json())
		.then(data => {
			const newProducts = data.slice(0, 8)
			setTopItems(newProducts.map(product => {
				return(
					<SwiperSlide>
					<ProductSlide key={product._id} product = {product}/> 
					</SwiperSlide>
				)	
			}))
			setLoading(false)
		})

	}, [])

	return(
		<>
		  <h5 className="text-center homeHeading">New Arrivals</h5>				
		  {(loading) ?
		  <Container className="mt-5">
		  	<Row className="justify-content-around">
		  		<LoadingSpinner />
		  		<LoadingSpinner />
		  		<LoadingSpinner />
		  	</Row>
		  </Container>
		  :	
		  <Container>
		  	<Row>
		  <Swiper
		  	onClick={toProduct}
		  	spaceBetween={0}
		    slidesPerView={1}
		    centeredSlides={false}
		    slidesPerGroupSkip={0}
		    grabCursor={true}
		    autoplay={{
		      delay: 3000,
		      disableOnInteraction: false,
		    }}
		    keyboard={{
		      enabled: true,
		    }}
		    breakpoints={{
		    	1024: {
		    	  slidesPerView: 4,
		    	  slidesPerGroup: 1,
		    	},
		    	768: {
		    	  slidesPerView: 3,
		    	  slidesPerGroup: 1,
		    	}
		    }}
		    scrollbar={true}
		    navigation={true}
		    pagination={{
		      clickable: true,
		    }}
		    modules={[Autoplay, Keyboard, Scrollbar, Navigation, Pagination]}
		    className="mySwiper"
		  >
		  	{topItems}
		  </Swiper>
		 	</Row>
		 </Container>
			}
		</>
	)
}