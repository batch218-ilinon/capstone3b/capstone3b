import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';

import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import Modal from 'react-bootstrap/Modal';

import Swal from 'sweetalert2';
import {useNavigate} from 'react-router-dom';

export default function ProductCard({product}) {

	const navigate = useNavigate();
	const [count, setCount] = useState(1);
	const [show, setShow] = useState(false);
	const [productFeatures, setProductFeatures] = useState([]);
	const features = product.features.split("\n")



	const { cartRefresh, setCartRefresh, user } = useContext(UserContext)

	function addCount(){
	  setCount(count + 1)
	  const localCart = JSON.parse(localStorage.getItem('cart'));
	  console.log(localCart)
	}

	function subtractCount(){
	  (count === 1)?
	  setCount(count)
	  :
	  setCount(count - 1)
	}

	function addToCart(){
		if(user.id !== null){
			console.log("working")
			fetch(`${process.env.REACT_APP_API_URL}/cart/${product._id}`, {
			  method: 'POST',
			  headers: {
			    'Content-Type': 'application/json',
			    Authorization: `Bearer ${localStorage.getItem('token')}`
			  },
			  body: JSON.stringify({
			    "quantity": count
			  })
			})  
			.then(res => res.json())
			.then(data => {
			  if(data === true){
			    Swal.fire({
			      position: 'center',
			      icon: 'success',
			      title: 'Added to Cart',
			      showConfirmButton: false,
			      timer: 1500
			    })
			    setCount(1)
			  }
			  else{
			    alert("Error")
			  }
			  setCartRefresh(current => !cartRefresh)
			})
		}
		else{
			const localCart = JSON.parse(localStorage.getItem('cart'));
			if(localCart === null){
				console.log(1)
				let localProduct = product
				localProduct.quantity = count
				localStorage.setItem('cart', JSON.stringify([localProduct]));
				Swal.fire({
				  position: 'center',
				  icon: 'success',
				  title: 'Added to Cart',
				  showConfirmButton: false,
				  timer: 1500
				})
			}
			else{
				console.log(2)
				let localCartTemp = localCart
				let localProduct = product
				let match = false
				for(let index = 0; index < localCartTemp.length; index++){
					if(localCartTemp[index]._id === product._id){
						console.log(localCart[index]._id)
						console.log(product._id)
						localCartTemp[index].quantity = localCartTemp[index].quantity + count
						Swal.fire({
						  position: 'center',
						  icon: 'success',
						  title: `Added to cart quantity updated to ${localCartTemp[index].quantity}`,
						  showConfirmButton: false,
						  timer: 1500
						})
						match = true

					}
				}
				if(!match){
					localProduct.quantity = count
					localCartTemp.push(localProduct)
					Swal.fire({
					  position: 'center',
					  icon: 'success',
					  title: 'Added to Cart',
					  showConfirmButton: false,
					  timer: 1500
					})
				}
				localStorage.setItem('cart', JSON.stringify(localCartTemp));
				setCartRefresh(current => !cartRefresh)
			}
		}
		setCount(1)
	}

	function createOrder(){
	  if(user.id !== null){
	  	fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
	  	  method: 'POST',
	  	  headers: {
	  	    'Content-Type': 'application/json',
	  	    Authorization: `Bearer ${localStorage.getItem('token')}`
	  	  },
	  	  body: JSON.stringify([{
	  	    "productId": product._id,
	  	    "quantity": count
	  	  }])
	  	})  
	  	.then(res => res.json())
	  	.then(data => {
	  	  console.log(data)
	  	  if(data === true){
	  	    Swal.fire({
	  	      position: 'center',
	  	      icon: 'success',
	  	      title: 'Order Created',
	  	      showConfirmButton: false,
	  	      timer: 1500
	  	    })
	  	    setCount(1)
	  	  }
	  	  else{
	  	    alert("Error")
	  	  }
	  	})
	  }
	  else{
	    navigate('/login')
	  }
	}

	useEffect(() => {
	  setProductFeatures(features.map((feature, index) => {
	    return (
	      <div key={index} className="singleProductFeatures">
	      {feature}
	      </div>
	    )
	  }))  
	}, [])


	return(
		<Col md={6} lg={4} className="px-lg-0">
			<Modal
			  size="xl"
			  centered
			  show={show}
			  onHide={() => setShow(false)}
			  dialogClassName="singleProductModal"
			  aria-labelledby="example-custom-modal-styling-title"
			>
			<Modal.Header closeButton>
			</Modal.Header>
			  <Modal.Body>
			  	<Container>
			  		<Row>
			  			<Col xs={12} lg={4} xl={5} className="text-center">
						  	<img className="singleProductImage" src={product.imgSrc} alt="Product"/>
						</Col>
						<Col >
						    <div className="singleProductName mt-5">
						    	{product.name}
						    </div>
						    <div className="singleProductDescription">
						    	{product.description}
						    </div>
						    <div className="singleProductFeatureHeading mt-3">
						    	Features
						    </div>
						    <div>
						    	{productFeatures}
						    </div>
						    <div className="singleProductPrice my-4">
						    	&#8369; {product.price}
						    </div>
						    	<Row className="justify-content-center align-items-center">
						    		<Col xs={6} lg={4} className="text-center">
						    			<Button className="mainAddButton py-0 mr-2 my-2" onClick={addCount} variant="secondary">+</Button>
						    			{count}
						    			<Button className="mainSubtractButton py-0 ml-2 my-2" onClick={subtractCount} variant="secondary">-</Button>
						    		</Col>
						    		<Col xs={12} lg={4} className="justify-content-center">
						    			<Row className="mr-lg-1">
						    			 <Button className="buyButton my-2" onClick={createOrder} variant="info">Buy</Button>
						    			 </Row>
						    		</Col>
						    		<Col xs={12} lg={4}>
						    			<Row>
						    			<Button className="addToCartButton my-2" onClick={addToCart} variant="info">&#128722; Add to Cart</Button>
						    			</Row>
						    		</Col>						 									   
						      	</Row>
			    		</Col>
			    	</Row>
			    </Container>
			  </Modal.Body>
			</Modal>

			<Card onClick={() => setShow(true)} border="light" className="text-center productCard">
			  <Card.Img className="productImageList" variant="top" src={product.imgSrc} />
			  <Card.Body>
			    <Card.Text className="productCardName">
			      {product.name}
			    </Card.Text>
			    <Card.Text className="productPrice">
			      &#8369; {product.price}
			    </Card.Text>
			  </Card.Body>
			</Card>
		</Col>

	)
}