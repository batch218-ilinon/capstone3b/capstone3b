import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Accordion from 'react-bootstrap/Accordion';

import envelope from '../icons/envelope.svg';
import telephone from '../icons/telephone.svg';

export default function Footer(){
	return(
		<>
		<Container fluid className="footerWrapper">
			<Row className="d-md-none">
				<Accordion>
				      <Accordion.Item eventKey="0">
				        <Accordion.Header>Our Company</Accordion.Header>
				        <Accordion.Body>
				          <div className="footerPassiveLink">Terms and conditions</div>
				          <div className="footerPassiveLink">About us</div>
				          <div className="footerPassiveLink">Stores</div>
				        </Accordion.Body>
				      </Accordion.Item>
				      <Accordion.Item eventKey="1">
				        <Accordion.Header>Contact Us</Accordion.Header>
				        <Accordion.Body>
					        <div className="footerPassiveLink"><img src={telephone} />(+632)999****</div>
					        <div className="footerPassiveLink"><img src={envelope} />topshop@mail.com</div>
				        </Accordion.Body>
				      </Accordion.Item>
				      <Accordion.Item eventKey="2">
				        <Accordion.Header>Account</Accordion.Header>
				        <Accordion.Body>
					        <a href="/login">
					        <div className="footerActiveLink">Login</div>
					        </a>
					        <a href="/register">
					        <div className="footerActiveLink">Create Account</div>
					        </a>
				        </Accordion.Body>
				      </Accordion.Item>
				    </Accordion>
				    <Row className="text-center footer ">
				    	<Col>
				    	<div className="footerLabel">&#169; 2023 TopShop All Rights Reserved.</div>
				    	</Col>
				    </Row>
			</Row>
		</Container>
		<Container fluid className="footerWrapper d-none d-md-block">
			<Row className="justify-content-center footerContent ">
				<Col md={3} lg={3} xl={2} >
					<h6>Our Company</h6>
						<div className="footerPassiveLink">Terms and conditions</div>
						<div className="footerPassiveLink">About us</div>
						<div className="footerPassiveLink">Stores</div>
				</Col>
				<Col md={3} lg={3} xl={2}>
					<h6>Contact Us</h6>
						<div className="footerPassiveLink"><img src={telephone} />(+632)999****</div>
						<div className="footerPassiveLink"><img src={envelope} />topshop@mail.com</div>
				</Col>
				<Col md={3} lg={3} xl={2}>
					<h6>Account</h6>
					<a href="/login">
					<div className="footerActiveLink">Login</div>
					</a>
					<a href="/register">
					<div className="footerActiveLink">Create Account</div>
					</a>
				</Col>
			</Row>
			<Row className="text-center footer p-0">
				<div className="footerLabel">&#169; 2023 TopShop All Rights Reserved.</div>
			</Row>
	</Container>
	</>
	)
}