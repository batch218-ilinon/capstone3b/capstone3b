import storeLogo from '../images/storeLogo2.png';
import cartFill from '../icons/cartFill.svg';

import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';

import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Offcanvas from 'react-bootstrap/Offcanvas';

import CartItems from './CartItems'

import {useEffect, useContext} from 'react';
import UserContext from '../UserContext';

export default function Header(){
	const [cartProducts, setCartProducts] = useState();
	const [isActive, setIsActive] = useState(false);
	const [show, setShow] = useState(false);
	const [totalAmount, setTotalAmount] = useState(0)
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	const {cartRefresh, user} = useContext(UserContext)

	useEffect(() => {
		const localCart = JSON.parse(localStorage.getItem('cart'))
		console.log(user.id)
		if(user.id !== null){
			fetch(`${process.env.REACT_APP_API_URL}/cart`, {
			  headers: {
			    'Content-Type': 'application/json',
			    Authorization: `Bearer ${localStorage.getItem('token')}`
			  }
			})
			.then(res => res.json())
			.then(data => {
				if(data === false){
					setCartProducts(null)
					setTotalAmount(0)
					setIsActive(false)
				}
				else{
					setIsActive(true)
					setTotalAmount(data.totalAmount)
					setCartProducts(data.products.map(product => {
						return <CartItems key = {product.prodId}  product = {product}/>
					}))
				}
			})
		}
		else{
			if(localCart !== null){
				setCartProducts(localCart.map((product, index) => {
				  return (
				  	<CartItems key={product._id} product={product}/>
				  )
				}))
				setTotalAmount(() => {
					let total = 0
					for(let index = 0; index < localCart.length; index++){
						total = total + (localCart[index].price * localCart[index].quantity)
					}
					return total
				})
				setIsActive(true) 
			}
			else{
				setIsActive(false)
			} 
		}
	}, [cartRefresh, user])

	return(
	<>
		<Container fluid className="headerWrapper pb-1">
			<Row className="justify-content-between align-items-center" >
				<Col>
				</Col>
				<Col className="text-center">
					<a href="/">
			  		<img className="storeLogo" src={storeLogo} alt="Store logo" />
			  		</a>
			  	</Col>
			  	<Col className="text-end pr-0 mr-5">
			  		<Row className="justify-content-end">
			  		<Col xs={2}>
			  		<img className="cartIcon" src={cartFill} onClick={handleShow} alt="Cart icon" />
			  		</Col>
			  		<Col xs={2}>
				  		<DropdownButton title="" drop="down-centered" id="dropdown-account-icon">
				  		  {(user.id !== null)?
				  		  <>
				  		  <Dropdown.Item href="/profile">My Account</Dropdown.Item>
				  		  <Dropdown.Item href="/logout">Logout</Dropdown.Item>
				  		  </>
				  		  :
				  		  <>
				  		  <Dropdown.Item href="/login">Sign In</Dropdown.Item>
				  		  <Dropdown.Item href="/register">Register</Dropdown.Item>
				  		  </>
				  		  }
				  		</DropdownButton>
			  		</Col>
			  		</Row>
			  	</Col>
		  	</Row>
		</Container>
		<Offcanvas show={show} onHide={handleClose} placement='end'>
		  <Offcanvas.Header closeButton>
		    <Offcanvas.Title className="cartTitle">Your Cart</Offcanvas.Title>
		  </Offcanvas.Header>
		  <Offcanvas.Body>
		  		{cartProducts}
		  </Offcanvas.Body>
		  <div>
		  	<Container>
		  		<Row className="justify-content-between">
		  			<Col sm={5} className="ml-3 cartTotalTitle">
		  				Total Amount
		  			</Col>
		  			<Col sm={4} className="cartTotalAmount">
		  				&#8369; {totalAmount}
		  			</Col>
		  		</Row>
		  		<Row className="justify-content-center">
		  			{ isActive ?
		  			<Button className="checkoutButton mb-4 mt-2">Checkout</Button>
		  			:
		  			<Button className="checkoutButton mb-4 mt-2" disabled>Checkout</Button>
		  			}
		  		</Row>
		  	</Container>
		  </div>
		</Offcanvas>
	</>
	)
}