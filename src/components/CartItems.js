import trashFill from '../icons/trashFill.svg';

import {useState, useContext} from 'react';
import UserContext from '../UserContext';

import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

export default function CartItems({product}){

	const [count, setCount] = useState(product.quantity);
	const [subTotal] = useState(product.quantity * product.price)
	const { setCartRefresh, user } = useContext(UserContext)

	function removeProduct(){
		if(user.id !== null){
			fetch(`${process.env.REACT_APP_API_URL}/cart/${product.productId}/remove`, {
			  method: 'POST',
			  headers: {
			    'Content-Type': 'application/json',
			    Authorization: `Bearer ${localStorage.getItem('token')}`
			  }
			})  
			.then(res => res.json())
			.then(data => {
			  console.log(typeof data)
			  if(data === true){
			    setCartRefresh()
			  }
			  else{
			    console.log(data.message)
			  }
			})
			setCartRefresh(current => !current)
		}
		else{
			const localCart = JSON.parse(localStorage.getItem('cart'))
			let localCartTemp = localCart
			for(let index = 0; index < localCartTemp.length; index++){
				if(localCartTemp[index]._id === product._id){
					localCartTemp.splice(index, 1)
				}
			}
			localStorage.setItem('cart', JSON.stringify(localCartTemp));
			setCartRefresh(current => !current)
		}
	}

	return(
		  <Container>
		  	<Row className="align-items-center">
		  		<Col xs={1} className="removeProductWrapper">
		  			<img onClick={removeProduct} src={trashFill} alt="Trashbin icon"/>
		  		</Col>
		  		<Col xs={4}>
		  			<img className="cartProductImage" src={product.imgSrc} alt="Product"/>
		  		</Col>
		  		<Col xs={7}>
				  	<div className="cartProductName">
				  		{product.name}
				  	</div>
				  	<div className="cartProductPrice pt-2">
				  		{count} x &#8369; {product.price}
				  	</div>
				  	<div className="cartProductSubtotal">
				  		Subtotal: &#8369; {subTotal}
				  	</div>
		  		</Col>
		  	</Row>
		  </Container>
	)
}